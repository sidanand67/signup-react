import React, { Component } from "react";
import Input from "./Input";
import Button from "./Button";
import Joi from "joi";
import './Form.css'; 

class Form extends Component {
    constructor(props) {
        super(props);

        this.state = {
            firstName: "",
            firstNameError: undefined,
            lastName: "",
            lastNameError: undefined,
            age: undefined,
            ageError: undefined,
            gender: "Male",
            genderError: undefined, 
            email: "",
            emailError: undefined, 
            password: "",
            passwordError: undefined,
            repeatPassword: "",
            repeatPasswordError: undefined, 
            terms: false, 
        };
    }

    validateFirstName = (value) => {
        const JoiSchema = Joi.object({
            firstName: Joi.string().min(1).required(),
        });

        return JoiSchema.validate(value);
    };

    validateLastName = (value) => {
        const JoiSchema = Joi.object({
            lastName: Joi.string().min(1).required(),
        });

        return JoiSchema.validate(value);
    };

    validateAge = (value) => {
        const JoiSchema = Joi.object({
            age: Joi.number().min(1).max(100).required(),
        });

        return JoiSchema.validate(value);
    };

    validateEmail = value => {
        const JoiSchema = Joi.object({
            email: Joi.string().email({tlds: {allow: false}}).required() 
        });

        return JoiSchema.validate(value);
    }

    validatePassword = value => {
        const JoiSchema = Joi.object({
            password: Joi.string().min(8).alphanum().required(),
        });

        return JoiSchema.validate(value);
    }

    handleChange = (event) => {
        const elementName = event.target.name;
        let result;

        switch (elementName) {
            case "firstName":
                result = this.validateFirstName({
                    firstName: event.target.value,
                });
                if (!result.error) {
                    this.setState({ [elementName]: event.target.value });
                    this.setState({ firstNameError: null}); 
                } else {
                    this.setState({
                        firstNameError: result.error.details[0].message,
                    });
                }
                break;

            case "lastName":
                result = this.validateLastName({
                    lastName: event.target.value,
                });
                if (!result.error) {
                    this.setState({ [elementName]: event.target.value });
                    this.setState({ lastNameError: null }); 
                } else {
                    this.setState({
                        lastNameError: result.error.details[0].message,
                    });
                }
                break;

            case "age":
                result = this.validateAge({
                    age: event.target.value,
                });
                if (!result.error) {
                    this.setState({ [elementName]: event.target.value });
                    this.setState({ ageError: null }); 
                } else {
                    this.setState({
                        ageError: result.error.details[0].message,
                    });
                }
                break;

            case 'email': 
                result = this.validateEmail({
                    email: event.target.value,
                });
                if (!result.error) {
                    this.setState({ [elementName]: event.target.value });
                    this.setState({ emailError: null}); 
                } else {
                    this.setState({
                        emailError: result.error.details[0].message,
                    });
                }
                break;

            case 'password': 
                result = this.validatePassword({
                    password: event.target.value,
                });
                if (!result.error) {
                    this.setState({ [elementName]: event.target.value });
                    this.setState({ passwordError: null}); 
                } else {
                    this.setState({
                        passwordError: result.error.details[0].message,
                    });
                }
                break;

            case 'repeatPassword': 
                if (event.target.value.length > 0 && event.target.value !== this.state.password){
                    this.setState({
                        repeatPasswordError: "Passwords do not match"
                    })
                }
                else {
                    this.setState({
                        repeatPasswordError: null,
                    });
                }
                break; 
        }
    };

    handleSubmit = (event) => {
        event.preventDefault();
        if (this.state.firstNameError === null && this.state.lastNameError === null && this.state.ageError === null && this.state.emailError === null && this.state.passwordError === null && this.state.repeatPasswordError === null){
            this.setState({error: false}); 
        }
    };

    render() {
        return (
            <form onSubmit={this.handleSubmit} className="form">
                <div className="input-tag">
                    <label htmlFor="firstName">
                        First Name
                        <Input
                            type={"text"}
                            placeholder={"Enter your first name"}
                            id={"firstName"}
                            name={"firstName"}
                            onChange={this.handleChange}
                        />
                    </label>
                    <p className="error">{this.state.firstNameError}</p>
                </div>

                <div className="input-tag">
                    <label htmlFor="lastName">
                        Last Name
                        <Input
                            type={"text"}
                            placeholder={"Enter your last name"}
                            id={"lastName"}
                            name={"lastName"}
                            onChange={this.handleChange}
                        />
                    </label>
                    <p className="error">{this.state.lastNameError}</p>
                </div>

                <div className="input-tag">
                    <label htmlFor="age">
                        Age
                        <Input
                            type={"text"}
                            placeholder={"Enter your age here"}
                            id={"age"}
                            name={"age"}
                            onChange={this.handleChange}
                        />
                    </label>
                    <p className="error">{this.state.ageError}</p>
                </div>

                <div className="input-tag">
                    Gender
                    <label htmlFor="male">
                        <Input
                            type={"radio"}
                            id={"male"}
                            name={"gender"}
                            checked={"true"}
                            onChange={this.onValueChange}
                        />
                        Male
                    </label>
                    <label htmlFor="female">
                        <Input
                            type={"radio"}
                            id={"female"}
                            name={"gender"}
                            onChange={this.onValueChange}
                        />
                        Female
                    </label>
                    <label htmlFor="others">
                        <Input
                            type={"radio"}
                            id={"others"}
                            name={"gender"}
                            onChange={this.onValueChange}
                        />
                        Others
                    </label>
                </div>

                <div className="input-tag">
                    <label htmlFor="role">
                        Role
                        <select className="dropdown">
                            <option value="developer">Developer</option>
                            <option value="seniorDeveloper">
                                Senior Developer
                            </option>
                            <option value="leadEngineer">Lead Engineer</option>
                            <option value="cto">CTO</option>
                        </select>
                    </label>
                </div>

                <div className="input-tag">
                    <label htmlFor="email">
                        Email
                        <Input
                            type={"email"}
                            placeholder={"Enter your email here"}
                            id={"email"}
                            name={"email"}
                            onChange={this.handleChange}
                        />
                    </label>
                    <p className="error">{this.state.emailError}</p>
                </div>

                <div className="input-tag">
                    <label htmlFor="password">
                        Password
                        <Input
                            type={"password"}
                            placeholder={"Enter your password here"}
                            id={"password"}
                            name={"password"}
                            onChange={this.handleChange}
                        />
                    </label>
                    <p className="error">{this.state.passwordError}</p>
                </div>

                <div className="input-tag">
                    <label htmlFor="repeatPassword">
                        Repeat Password
                        <Input
                            type={"password"}
                            placeholder={"Confirm your password"}
                            id={"repeatPassword"}
                            name={"repeatPassword"}
                            onChange={this.handleChange}
                        />
                    </label>
                    <p className="error">{this.state.repeatPasswordError}</p>
                </div>

                <div className="input-tag">
                    <label htmlFor="terms">
                        <Input
                            type={"checkbox"}
                            id={"terms"}
                            name={"terms"}
                            onChange={this.handleChange}
                        />
                        I agree to terms &amp; conditions.
                    </label>
                </div>

                <div className="input-tag">
                    <Button type={"submit"} value={"Sign Up"} />
                </div>

                <div>
                    {this.state.error === false ? <p className="success-msg">You've successfully created your account.</p> : null}
                </div>
            </form>
        );
    }
}

export default Form;