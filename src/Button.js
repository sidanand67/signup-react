import React from "react";
import './Button.css'; 

const Button = (props) => {
    return (
        <button type={props.type}>{props.value}</button>
    ); 
}

export default Button; 