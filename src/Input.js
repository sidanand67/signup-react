import React from "react";
import './input.css'; 

const Input = (props) => {
    return (
        <input
            type={props.type}
            placeholder={props.placeholder ? props.placeholder : ''}
            id={props.id}
            name={props.name}
            onInput={props.onChange}
            onBlur={props.onChange}
            className="input"
            checked={props.checked}
        />
    );
};

export default Input;
